function server() {
    var http = require("http");
    var url = require('url');
    var fs = require('fs');
    var createServer = http.createServer(function(req, res) {
        var q = url.parse(req.url, true);
        console.log(req.url);
        console.log(q.pathname);
        switch (q.pathname) {
            case ("/"):
                fs.readFile("/index.html", function(err, data) {
                    if (err) {
                        res.writeHead(404, { 'content-type': "text / html" })
                        return res.end("404 not found");
                    }
                    res.writeHead(200, { 'content-type': "text / html" })
                    res.write(data);
                    res.end();
                })
        }
    });
    createServer.listen(8000);
}
server();